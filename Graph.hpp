// ---------
// Graph.hpp
// ----------

#ifndef Graph_hpp
#define Graph_hpp

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <set>     // set
#include <utility> // make_pair, pair
#include <vector>  // vector

// -----
// Graph
// -----

class Graph {
private:
    static int graph_counter;

public:
    // ------
    // usings
    // ------

    using vertex_descriptor  = int;
    using edge_descriptor    = std::pair<vertex_descriptor, vertex_descriptor>;

    // Regular bidirectional iterator
    class vertex_iterator {
        friend class Graph;
        private:
            const Graph* _g;
            vertex_descriptor _vertex_begin;
            vertex_descriptor _vertex;
            vertex_descriptor _vertex_end;

        public:
            vertex_iterator() : _g(nullptr) {};

            vertex_iterator(const Graph& g) : _g(&g) {
                _vertex = _vertex_begin = 0;
                _vertex_end = num_vertices(*_g);
            }
            vertex_iterator(const Graph& g, vertex_descriptor v) : vertex_iterator(g) {
                _vertex = v;
            }

            vertex_iterator(const vertex_iterator&) = default;
            vertex_iterator& operator= (const vertex_iterator& v) {
                _g = v._g;
                _vertex_begin = v._vertex_begin;
                _vertex = v._vertex;
                _vertex_end = v._vertex_end;
                return *this;
            };

            friend bool operator == (const vertex_iterator& a, const vertex_iterator& b) {
                return a._g == b._g && a._vertex == b._vertex;
            }
            friend bool operator != (const vertex_iterator& a, const vertex_iterator& b) {
                return !(a == b);
            }

            vertex_descriptor& operator * () {
                return _vertex;
            }
            vertex_descriptor* operator -> () {
                return &**this;
            }

            vertex_iterator& operator ++ () {
                if (_vertex != _vertex_end)
                    ++_vertex;
                return *this;
            }
            vertex_iterator operator ++ (int) {
                vertex_iterator x = *this;
                ++(*this);
                return x;
            }

            vertex_iterator& operator -- () {
                if (_vertex != _vertex_begin)
                    --_vertex;
                return *this;
            }
            vertex_iterator operator -- (int) {
                vertex_iterator x = *this;
                --(*this);
                return x;
            }
    };
    using adjacency_iterator = std::set<vertex_descriptor>::iterator;

    // Bidirectional iterator that relies on vertex and adjacency iterator
    class edge_iterator {
        private:
            const Graph* _g;
            vertex_iterator _vertex_begin;
            vertex_iterator _vertex;
            vertex_iterator _vertex_end;
            adjacency_iterator _adj_vertex_begin;
            adjacency_iterator _adj_vertex;
            adjacency_iterator _adj_vertex_end;
            edge_descriptor _val;

        public:
            edge_iterator() : _g(nullptr) {};

            edge_iterator(const Graph& g, bool beginning) : _g(&g) {
                if (beginning) {
                    // member variables set to the very first vertex & it's first adjacent vertex
                    std::pair<vertex_iterator, vertex_iterator> ret1 = vertices(*_g);
                    _vertex = _vertex_begin = ret1.first;
                    _vertex_end = ret1.second;

                    // The edge iterator needs to keep moving forward until it finds an existing source and target
                    if (_vertex != _vertex_end) {
                        std::pair<adjacency_iterator, adjacency_iterator> ret2 = adjacent_vertices(*_vertex, *_g);
                        _adj_vertex = _adj_vertex_begin = ret2.first;
                        _adj_vertex_end = ret2.second;

                        while (_vertex != _vertex_end && _adj_vertex == _adj_vertex_end) {
                            ++_vertex;
                            if (_vertex == _vertex_end)
                                break;
                            ret2 = adjacent_vertices(*_vertex, *_g);
                            _adj_vertex = _adj_vertex_begin = ret2.first;
                            _adj_vertex_end = ret2.second;
                        }
                        if (_vertex != _vertex_end)
                            _val = std::make_pair(*_vertex, *_adj_vertex);
                    }
                } else {
                    std::pair<vertex_iterator, vertex_iterator> ret1 = vertices(*_g);
                    _vertex_begin = ret1.first;
                    _vertex = _vertex_end = ret1.second;
                }
            };

            edge_iterator (const edge_iterator&) = default;

            edge_iterator& operator= (const edge_iterator&) = default;

            friend bool operator == (const edge_iterator& a, const edge_iterator& b) {
                if (a._g != b._g)
                    return false;
                if (a._vertex != b._vertex)
                    return false;
                if (a._vertex == a._vertex_end && b._vertex == b._vertex_end)
                    return true;
                return a._adj_vertex == b._adj_vertex;
            }
            friend bool operator != (const edge_iterator& lhs, const edge_iterator& rhs) {
                return !(lhs == rhs);
            }

            edge_descriptor operator * () {
                return _val;
            }

            edge_iterator& operator ++ () {
                // Keep moving forward until an existing edge is found
                if (_vertex != _vertex_end) {
                    ++_adj_vertex;
                    while (_vertex != _vertex_end && _adj_vertex == _adj_vertex_end) {
                        ++_vertex;
                        if (_vertex == _vertex_end)
                            break;
                        std::pair<adjacency_iterator, adjacency_iterator> ret2 = adjacent_vertices(*_vertex, *_g);
                        _adj_vertex = _adj_vertex_begin = ret2.first;
                        _adj_vertex_end = ret2.second;
                    }
                    if (_vertex != _vertex_end)
                        _val = std::make_pair(*_vertex, *_adj_vertex);
                }
                return *this;
            }
            edge_iterator operator ++ (int) {
                edge_iterator x = *this;
                ++(*this);
                return x;
            }

            edge_iterator& operator -- () {
                if (_vertex == _vertex_begin && _adj_vertex == _adj_vertex_begin)
                    return *this;
                if (_vertex != _vertex_end && _adj_vertex != _adj_vertex_begin) {
                    --_adj_vertex;
                    _val = std::make_pair(*_vertex, *_adj_vertex);
                    return *this;
                }

                // Need to move backwards until an existing edge is found
                std::pair<adjacency_iterator, adjacency_iterator> ret2 = adjacent_vertices(*(--_vertex), *_g);
                _adj_vertex_begin = ret2.first;
                _adj_vertex = _adj_vertex_end = ret2.second;
                while (_vertex != _vertex_begin && _adj_vertex_begin == _adj_vertex_end) {
                    ret2 = adjacent_vertices(*(--_vertex), *_g);
                    _adj_vertex_begin = ret2.first;
                    _adj_vertex = _adj_vertex_end = ret2.second;
                }
                if (_adj_vertex_begin != _adj_vertex_end) {
                    --_adj_vertex;
                    _val = std::make_pair(*_vertex, *_adj_vertex);
                }
                return *this;
            }
            edge_iterator operator -- (int) {
                edge_iterator x = *this;
                --(*this);
                return x;
            }
    };

    using vertices_size_type = std::size_t;
    using edges_size_type    = std::size_t;

public:
    // --------
    // add_edge
    // --------

    /**
     * Adds an edge to the graph
     * Returns false as the second value if the edge already exists
     * Parallel edges are not allowed
     * Assumes that both vertices exist
     */
    friend std::pair<edge_descriptor, bool> add_edge (vertex_descriptor a, vertex_descriptor b, Graph& g) {
        std::pair<edge_descriptor, bool> exists = edge(a, b, g);
        if (exists.second)
            return std::make_pair(exists.first, false);

        g._data.at(a).insert(b);
        ++g.num_edges;
        g.valid();
        return std::make_pair(std::make_pair(a, b), true);
    }

    // ----------
    // add_vertex
    // ----------

    /**
     * Add's a vertex to the graph and builds it's corresponding set
     */
    friend vertex_descriptor add_vertex (Graph& g) {
        vertex_descriptor v = num_vertices(g);
        g._data.emplace(g._data.end());
        g.valid();
        return v;
    }

    // -----------------
    // adjacent_vertices
    // -----------------

    /**
     * Returns a beginning and end iterator to the adjacent vertices for the provided vertex. 
     * Assumes the vertex already exists
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (vertex_descriptor a, const Graph& g) {
        return std::make_pair(g._data.at(a).begin(), g._data.at(a).end());
    }

    // ----
    // edge
    // ----

    /**
     * Retrieves the edge between the two vertices. 
     * The second argument represents whether the vertex exists or not.
     * Assumes the vertices exist. 
     */
    friend std::pair<edge_descriptor, bool> edge (vertex_descriptor a, vertex_descriptor b, const Graph& g) {
        return std::make_pair(std::make_pair(a, b), g._data.at(a).count(b));
    }

    // -----
    // edges
    // -----

    /**
     * Returns a beginning and end bidirectional iterator for the graph provided. 
     */
    friend std::pair<edge_iterator, edge_iterator> edges (const Graph& g) {
        return std::make_pair(edge_iterator(g, true), edge_iterator(g, false));
    }

    // ---------
    // num_edges
    // ---------

    /**
     * Returns the number of edges in the graph. 
     */
    friend edges_size_type num_edges (const Graph& g) {
        return g.num_edges;
    }

    // ------------
    // num_vertices
    // ------------

    /**
     * Returns the number of vertices in the graph. 
     */
    friend vertices_size_type num_vertices (const Graph& g) {
        return g._data.size();
    }

    // ------
    // source
    // ------

    /**
     * Returns the source of the provided edge. 
     * Assumes the edge matches up to the given graph. 
     * Does not rely on provided graph since edge_descriptor holds the vertices. 
     */
    friend vertex_descriptor source (edge_descriptor ed, const Graph&) {
        return ed.first;
    }

    // ------
    // target
    // ------

    /**
     * Returns the target of the provided edge. 
     * Assumes the edge matches up to the given graph. 
     * Does not rely on provided graph since edge_descriptor holds the vertices. 
     */
    friend vertex_descriptor target (edge_descriptor ed, const Graph&) {
        return ed.second;
    }

    // ------
    // vertex
    // ------

    /**
     * Retrieves the correponding vertex given an index and a graph. 
     * Assumes the index and the graph match up. 
     * Vertex descriptors are indices so this does not rely on the graph. 
     */
    friend vertex_descriptor vertex (vertices_size_type n, const Graph&) {
        return n;
    }

    // --------
    // vertices
    // --------

    /**
     * Returns a beginning and end iterator to the vertices of a graph
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& g) {
        return std::make_pair(vertex_iterator(g), vertex_iterator(g, num_vertices(g)));
    }


    // Helper functions used by iterators to ensure iterators are from the same graph. 
    friend bool operator == (const Graph& a, const Graph& b) {
        return a._graph_id == b._graph_id;
    }
    friend bool operator != (const Graph& a, const Graph& b) {
        return !(a._graph_id == b._graph_id);
    }

private:
    // ----
    // data
    // ----

    // Each unique graph has its own id unless copied
    int _graph_id;
    std::vector<std::set<vertex_descriptor>> _data;

    // Separate variable for number of edges so that num_edges(Graph) is O(1)
    edges_size_type num_edges = 0;

    // -----
    // valid
    // -----

    /**
     * your documentation
     */
    bool valid () const {
        assert(_graph_id > 0);
        assert(_graph_id <= Graph::graph_counter);

        // Validity testing takes too long otherwise
        if (num_vertices(*this) < 50) {
            edges_size_type ne = 0;
            for (std::set<vertex_descriptor> x : _data) {
                assert(x.size() <= _data.size());
                ne += x.size();
            }
            assert(ne == num_edges);
        }
        return true;
    }

public:
    // --------
    // defaults
    // --------

    Graph() {
        _graph_id = ++Graph::graph_counter;
    }
    Graph             (const Graph&) = default;
    ~Graph            ()             = default;
    Graph& operator = (const Graph&) = default;
};

int Graph::graph_counter;
#endif // Graph_hpp
