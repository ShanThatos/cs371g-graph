// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;

    static tuple<graph_type, vector<vertex_descriptor>, vector<pair<vertex_descriptor, vertex_descriptor>>> make_graph(int num_verts, int num_eds, unsigned pseudo_random = 73475843) {
        graph_type g;

        vector<vertex_descriptor> vertices;
        for (int nv = 0; nv < num_verts; ++nv)
            vertices.push_back(add_vertex(g));

        vector<pair<vertex_descriptor, vertex_descriptor>> possibleEdges;
        vector<pair<vertex_descriptor, vertex_descriptor>> edges;
        for (unsigned i = 0; i < vertices.size(); ++i)
            for (unsigned j = 0; j < vertices.size(); ++j)
                possibleEdges.push_back(make_pair(vertices[i], vertices[j]));

        pseudo_random %= possibleEdges.size();
        for (int ne = 0; ne < num_eds; ++ne) {
            int edgeIndex = pseudo_random % possibleEdges.size();
            pseudo_random *= pseudo_random * 1331;
            pseudo_random %= possibleEdges.size();

            pair<vertex_descriptor, vertex_descriptor> x = possibleEdges[edgeIndex];
            possibleEdges.erase(possibleEdges.begin() + edgeIndex);

            add_edge(x.first, x.second, g);
            edges.push_back(x);
        }

        return make_tuple(g, vertices, edges);
    }
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment, add a comma to the line above
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

// Original Tests

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// ---- My Tests ----

// -- Empty Graph Tests --
// Testing num verts/edges
TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0u);
    ASSERT_EQ(num_edges(g), 0u);
}

// Testing edges() and vertices()
TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> ret = vertices(g);
    ASSERT_EQ(ret.first, ret.second);

    pair<edge_iterator, edge_iterator> ret2 = edges(g);
    ASSERT_EQ(ret2.first, ret2.second);
}

// Testing copies of empty graph
TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type og;
    graph_type all_gs[] = {graph_type(og), graph_type()};
    all_gs[1] = og;

    for (graph_type g : all_gs) {
        ASSERT_EQ(num_vertices(g), 0u);
        ASSERT_EQ(num_edges(g), 0u);

        pair<vertex_iterator, vertex_iterator> ret = vertices(g);
        ASSERT_EQ(ret.first, ret.second);

        pair<edge_iterator, edge_iterator> ret2 = edges(g);
        ASSERT_EQ(ret2.first, ret2.second);
    }
}

// -- No Edges --
// Single Vertex
TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor va = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 1u);
    ASSERT_EQ(num_edges(g), 0);
    ASSERT_EQ(va, vertex(0, g));

    pair<vertex_iterator, vertex_iterator> ret = vertices(g);
    ASSERT_NE(ret.first, ret.second);
    ASSERT_EQ(va, *ret.first);

    vertex_iterator b = ret.first;
    b++;
    ASSERT_EQ(b, ret.second);
    ASSERT_NE(va, *b);

    pair<edge_iterator, edge_iterator> ret2 = edges(g);

    ASSERT_EQ(ret2.first, ret2.second);

}

// A few vertices
TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor va = add_vertex(g);
    vertex_descriptor vb = add_vertex(g);
    vertex_descriptor vc = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 3u);
    ASSERT_EQ(num_edges(g), 0);
    ASSERT_EQ(va, vertex(0, g));
    ASSERT_EQ(vb, vertex(1, g));
    ASSERT_EQ(vc, vertex(2, g));

    pair<vertex_iterator, vertex_iterator> ret = vertices(g);
    vertex_iterator b = ret.first;
    vertex_iterator e = ret.second;
    ASSERT_EQ(va, *b);
    ASSERT_NE(b, e);

    b++;
    ASSERT_EQ(vb, *b);
    ASSERT_NE(b, e);

    b++;
    ASSERT_EQ(vc, *b);
    ASSERT_NE(b, e);

    b++;
    ASSERT_EQ(b, e);

    pair<edge_iterator, edge_iterator> ret2 = edges(g);
    ASSERT_EQ(ret2.first, ret2.second);
}

// 100 vertices
TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    using namespace std;
    vector<vertex_descriptor> v;

    while (v.size() < 100)
        v.push_back(add_vertex(g));

    ASSERT_EQ(num_vertices(g), 100u);
    ASSERT_EQ(num_edges(g), 0u);

    for (unsigned i = 0; i < 100; ++i)
        ASSERT_EQ(v[i], vertex(i, g));

    pair<vertex_iterator, vertex_iterator> ret = vertices(g);
    vertex_iterator b = ret.first;
    vertex_iterator e = ret.second;

    for (unsigned i = 0; i < 100; ++i, ++b) {
        ASSERT_EQ(v[i], *b);
        ASSERT_NE(b, e);
    }
    ASSERT_EQ(b, e);

    pair<edge_iterator, edge_iterator> ret2 = edges(g);
    ASSERT_EQ(ret2.first, ret2.second);
}

// -- 2 vertices, 1 edge --
// Testing vertices unchanged
TYPED_TEST(GraphFixture, test11) {
    auto [ g, vertices, edges ] = TestFixture::make_graph(2, 1);
    ASSERT_EQ(num_vertices(g), 2u);
    ASSERT_EQ(num_edges(g), 1u);

    for (unsigned i = 0; i < vertices.size(); ++i)
        ASSERT_EQ(vertex(i, g), vertices[i]);
}

// Testing vertex iterator
TYPED_TEST(GraphFixture, test12) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 1);

    auto [ b, e ] = vertices(g);
    for (unsigned i = 0; i < verts.size(); ++i, ++b)
        ASSERT_EQ(verts[i], *b);
    ASSERT_EQ(b, e);
}

// Testing edge exists
TYPED_TEST(GraphFixture, test13) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 1);

    auto [ edge_res, exists ] = edge(edgs[0].first, edgs[0].second, g);
    ASSERT_EQ(exists, true);
}

// Testing add_edge fails
TYPED_TEST(GraphFixture, test14) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 1);

    auto [ edge_res, exists ] = add_edge(edgs[0].first, edgs[0].second, g);
    ASSERT_EQ(exists, false);
}

// Testing adjacent_vertices
TYPED_TEST(GraphFixture, test15) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 1);

    auto [ b, e ] = adjacent_vertices(edgs[0].first, g);
    ASSERT_EQ(*b, edgs[0].second);
    ++b;
    ASSERT_EQ(b, e);
}

// Testing edges + iterator, source & target
TYPED_TEST(GraphFixture, test16) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 1);

    auto [ b, e ] = edges(g);
    ASSERT_EQ(source(*b, g), edgs[0].first);
    ASSERT_EQ(target(*b, g), edgs[0].second);

    ++b;
    ASSERT_EQ(b, e);
}

// -- 2 vertices, 4 edges --
// Testing vertices unchanged
TYPED_TEST(GraphFixture, test17) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 4);
    ASSERT_EQ(num_vertices(g), 2u);
    ASSERT_EQ(num_edges(g), 4u);

    for (unsigned i = 0; i < verts.size(); ++i)
        ASSERT_EQ(vertex(i, g), verts[i]);
}

// Testing vertex iterator
TYPED_TEST(GraphFixture, test18) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 4);

    auto [ b, e ] = vertices(g);
    for (unsigned i = 0; i < verts.size(); ++i, ++b)
        ASSERT_EQ(verts[i], *b);
    ASSERT_EQ(b, e);
}

// Testing edge exists
TYPED_TEST(GraphFixture, test19) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 4);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, true);
    }
}

// Testing add_edge fails
TYPED_TEST(GraphFixture, test20) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 4);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = add_edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, false);
    }
}

// Testing adjacent_vertices
TYPED_TEST(GraphFixture, test21) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 4);

    for (auto ed : edgs) {
        auto [ b, e ] = adjacent_vertices(ed.first, g);
        ASSERT_EQ(count(b, e, ed.second), 1);
    }
}

// Testing edges + iterator, source & target
TYPED_TEST(GraphFixture, test22) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(2, 4);

    int nv = 0;
    auto [ b, e ] = edges(g);
    while (b != e) {
        auto va = source(*b, g);
        auto vb = target(*b, g);
        ASSERT_EQ(count(edgs.begin(), edgs.end(), make_pair(va, vb)), 1);
        ++b;
        ++nv;
    }
    ASSERT_EQ(b, e);
    ASSERT_EQ(nv, edgs.size());
}

// -- 50 vertices, 1 edge --
// Testing vertices unchanged
TYPED_TEST(GraphFixture, test23) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 1);
    ASSERT_EQ(num_vertices(g), 50u);
    ASSERT_EQ(num_edges(g), 1u);

    for (unsigned i = 0; i < verts.size(); ++i)
        ASSERT_EQ(vertex(i, g), verts[i]);
}

// Testing vertex iterator
TYPED_TEST(GraphFixture, test24) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 1);

    auto [ b, e ] = vertices(g);
    for (unsigned i = 0; i < verts.size(); ++i, ++b)
        ASSERT_EQ(verts[i], *b);
    ASSERT_EQ(b, e);
}

// Testing edge exists
TYPED_TEST(GraphFixture, test25) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 1);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, true);
    }
}

// Testing add_edge fails
TYPED_TEST(GraphFixture, test26) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 1);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = add_edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, false);
    }
}

// Testing adjacent_vertices
TYPED_TEST(GraphFixture, test27) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 1);

    for (auto ed : edgs) {
        auto [ b, e ] = adjacent_vertices(ed.first, g);
        ASSERT_EQ(count(b, e, ed.second), 1);
    }
}

// Testing edges + iterator, source & target
TYPED_TEST(GraphFixture, test28) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 1);

    int nv = 0;
    auto [ b, e ] = edges(g);
    while (b != e) {
        auto va = source(*b, g);
        auto vb = target(*b, g);
        ASSERT_EQ(count(edgs.begin(), edgs.end(), make_pair(va, vb)), 1);
        ++b;
        ++nv;
    }
    ASSERT_EQ(b, e);
    ASSERT_EQ(nv, edgs.size());
}

// -- 50 vertices, 100 edges --
// Testing vertices unchanged
TYPED_TEST(GraphFixture, test29) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 100);
    ASSERT_EQ(num_vertices(g), 50u);
    ASSERT_EQ(num_edges(g), 100u);

    for (unsigned i = 0; i < verts.size(); ++i)
        ASSERT_EQ(vertex(i, g), verts[i]);
}

// Testing vertex iterator
TYPED_TEST(GraphFixture, test30) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 100);

    auto [ b, e ] = vertices(g);
    for (unsigned i = 0; i < verts.size(); ++i, ++b)
        ASSERT_EQ(verts[i], *b);
    ASSERT_EQ(b, e);
}

// Testing edge exists
TYPED_TEST(GraphFixture, test31) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 100);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, true);
    }
}

// Testing add_edge fails
TYPED_TEST(GraphFixture, test32) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 100);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = add_edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, false);
    }
}

// Testing adjacent_vertices
TYPED_TEST(GraphFixture, test33) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 100);

    for (auto ed : edgs) {
        auto [ b, e ] = adjacent_vertices(ed.first, g);
        ASSERT_EQ(count(b, e, ed.second), 1);
    }
}

// Testing edges + iterator, source & target
TYPED_TEST(GraphFixture, test34) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(50, 100);

    int nv = 0;
    auto [ b, e ] = edges(g);
    while (b != e) {
        auto va = source(*b, g);
        auto vb = target(*b, g);
        ASSERT_EQ(count(edgs.begin(), edgs.end(), make_pair(va, vb)), 1);
        ++b;
        ++nv;
    }
    ASSERT_EQ(b, e);
    ASSERT_EQ(nv, edgs.size());
}

// -- 100 vertices, 10000 edges --
// Testing vertices unchanged
TYPED_TEST(GraphFixture, test35) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(100, 10000);
    ASSERT_EQ(num_vertices(g), 100u);
    ASSERT_EQ(num_edges(g), 10000u);

    for (unsigned i = 0; i < verts.size(); ++i)
        ASSERT_EQ(vertex(i, g), verts[i]);
}

// Testing vertex iterator
TYPED_TEST(GraphFixture, test36) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(100, 10000);

    auto [ b, e ] = vertices(g);
    for (unsigned i = 0; i < verts.size(); ++i, ++b)
        ASSERT_EQ(verts[i], *b);
    ASSERT_EQ(b, e);
}

// Testing edge exists
TYPED_TEST(GraphFixture, test37) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(100, 10000);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, true);
    }
}

// Testing add_edge fails
TYPED_TEST(GraphFixture, test38) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(100, 10000);

    for (auto ed : edgs) {
        auto [ edge_res, exists ] = add_edge(ed.first, ed.second, g);
        ASSERT_EQ(exists, false);
    }
}

// Testing adjacent_vertices
TYPED_TEST(GraphFixture, test39) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(100, 10000);

    for (auto ed : edgs) {
        auto [ b, e ] = adjacent_vertices(ed.first, g);
        ASSERT_EQ(count(b, e, ed.second), 1);
    }
}

// Testing edges + iterator, source & target
TYPED_TEST(GraphFixture, test40) {
    auto [ g, verts, edgs ] = TestFixture::make_graph(100, 10000);

    int nv = 0;
    auto [ b, e ] = edges(g);
    while (b != e) {
        auto va = source(*b, g);
        auto vb = target(*b, g);
        ASSERT_EQ(count(edgs.begin(), edgs.end(), make_pair(va, vb)), 1);
        ++b;
        ++nv;
    }
    ASSERT_EQ(b, e);
    ASSERT_EQ(nv, edgs.size());
}